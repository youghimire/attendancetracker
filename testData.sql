/*
SQLyog Community
MySQL - 10.1.35-MariaDB : Database - attendance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`attendance` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `attendance`;

/*Table structure for table `employee` */

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `salary` decimal(19,2) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `employer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrmknjkdeoo3molal1gxkt9dar` (`employer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee` */

insert  into `employee`(`id`,`name`,`password`,`role`,`salary`,`user_name`,`employer_id`) values 
(0,'Mr HR Manager','admin','admin',25000.00,'admin',0),
(1,'Ram Kumar','password',NULL,15000.00,'ramkr',0),
(2,'Hari Chandra','password',NULL,27000.00,'haric',0);

/*Table structure for table `employee_leave` */

CREATE TABLE `employee_leave` (
  `id` int(11) NOT NULL,
  `leave_count` int(11) DEFAULT NULL,
  `month` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqat82qeb4igyj2vrh07tif37w` (`employee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee_leave` */

insert  into `employee_leave`(`id`,`leave_count`,`month`,`employee_id`) values 
(0,3,'2019-08-02 14:31:33',1),
(1,2,'2019-07-19 14:32:00',1),
(2,1,'2019-06-18 14:32:27',1),
(3,4,'2019-04-17 14:32:53',1),
(4,2,'2019-08-04 14:33:37',2),
(5,6,'2019-08-01 00:00:00',1),
(6,3,'2019-09-01 00:00:00',1),
(7,1,'2019-09-01 00:00:00',0),
(9,2,'2019-08-14 00:00:00',0);

/*Table structure for table `employer` */

CREATE TABLE `employer` (
  `id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `leave_per_month` int(11) NOT NULL,
  `leave_per_year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employer` */

insert  into `employer`(`id`,`address`,`name`,`leave_per_month`,`leave_per_year`) values 
(0,'Thapathali','Test Corp',2,18);

/*Table structure for table `hibernate_sequence` */

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values 
(10),
(10),
(10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
