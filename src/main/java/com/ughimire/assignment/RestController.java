package com.ughimire.assignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RestController {

	@Autowired
	private ReportGenerator reportGenerator;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private WebSecurityConfig webSecurityConfig;

	@GetMapping("/")
	public String greet() {
		return "home";
	}

	@PostMapping("/report")
	public String report(@RequestParam("employeeSelected") String userName, @RequestParam("yearSelected") Integer year,
			Model model) {
		model.addAttribute("allEmployee", employeeService.getMyEmployee());
		reportGenerator.addReport(model, employeeService.findByUserName(userName), year);
		return "report";
	}

	@GetMapping("/report")
	public String report(Model model, Authentication authentication) {
		reportGenerator.addReport(model);
		model.addAttribute("allEmployee", employeeService.getMyEmployee());
		return "report";
	}

	@GetMapping("/entry")
	public String entry(Model model, Authentication authentication) {
		model.addAttribute("allEmployee", employeeService.getMyEmployee());
		return "entry";
	}

	@PostMapping(path = "/entry")
	public String infoPost(@RequestParam("employeeSelected") String userName,
			@RequestParam("startDate") String startDate, @RequestParam("leaveCount") Integer leaveCount, Model model)
			throws ParseException {
		model.addAttribute("allEmployee", employeeService.getMyEmployee());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		employeeService.setLeave(format.parse(startDate), leaveCount, userName);
		return "entry";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

}
