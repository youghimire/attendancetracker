package com.ughimire.assignment;

import java.math.BigDecimal;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String role;
	@ManyToOne
	@JoinColumn(name = "employer_id")
	Employer employer;
	private String userName;
	private String password;
	private BigDecimal salary;

	@OneToMany(mappedBy = "employee")
	List<EmployeeLeave> employeeLeaves;

	public int getLeave() {
		return getLeave(YearMonth.now());
	}

	/**
	 * Add all the leave recorded for this month and return it.
	 * @param yearMonth
	 * @return
	 */
	public int getLeave(YearMonth yearMonth) {
		int leaveCount = 0;
		List<EmployeeLeave> leaves = getEmployeeLeaves();
		for (EmployeeLeave leave : leaves) {
			YearMonth leaveMonth = YearMonth
					.from(leave.getMonth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

			if (leaveMonth.equals(yearMonth)) {
				leaveCount += leave.getLeaveCount();
			}
		}
		return leaveCount;

	}

}
