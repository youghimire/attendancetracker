package com.ughimire.assignment;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepositories extends CrudRepository<Employee, Integer> {

}
