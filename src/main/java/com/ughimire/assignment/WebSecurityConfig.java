package com.ughimire.assignment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	EmployeeRepositories employeeRepositories;

	/**
	 * Allow only home page for users who are not logged in.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/home").permitAll().anyRequest().authenticated().and().formLogin()
				.loginPage("/login").permitAll().and().logout().permitAll();
	}

	/**
	 * Add the employee who are set as admin on their role column as a web user.
	 */
	@Bean
	@Override
	public UserDetailsService userDetailsService() {

		Iterable<Employee> allEmployee = employeeRepositories.findAll();
		List<UserDetails> users = new ArrayList<>();
		for (Employee employee : allEmployee) {
			if ("admin".equalsIgnoreCase(employee.getRole())) {
				UserDetails user = User.withDefaultPasswordEncoder().username(employee.getUserName())
						.password(employee.getPassword()).roles(employee.getRole()).build();
				users.add(user);
			}
		}

		return new InMemoryUserDetailsManager(users);
	}
}
