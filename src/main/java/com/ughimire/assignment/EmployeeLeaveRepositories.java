package com.ughimire.assignment;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeLeaveRepositories extends CrudRepository<EmployeeLeave, Integer> {

}
