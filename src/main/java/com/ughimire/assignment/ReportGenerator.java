package com.ughimire.assignment;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class ReportGenerator {

	@Autowired
	EmployeeService employeeService;

	/**
	 * Generate a table and add that to the view model.
	 * @param employee For which the report should be publish
	 * @param year Year for which the report is generated
	 * @param model View model to publish
	 */
	public void addYearlyTable(Employee employee, int year, Model model) {

		List<List<String>> table = new ArrayList<>();

		int leavePerMonth = employee.getEmployer().getSetting().getLeavePerMonth();
		int leavePerYear = employee.getEmployer().getSetting().getLeavePerYear();

		int remainingLeaveAlloted = 0;
		YearMonth thisMonth = YearMonth.now();
		for (int month = 1; month <= 12; month++) {

			List<String> row = new ArrayList<>();

			remainingLeaveAlloted += leavePerMonth;
			if (remainingLeaveAlloted > leavePerYear) {
				remainingLeaveAlloted = leavePerYear;
			}

			YearMonth yearMonth = YearMonth.of(year, month);

			int leaveTaken = employee.getLeave(yearMonth);

			int extraLeave = 0;
			if (leaveTaken <= remainingLeaveAlloted) {
				remainingLeaveAlloted = remainingLeaveAlloted - leaveTaken;
			} else if (leaveTaken > remainingLeaveAlloted) {
				extraLeave = leaveTaken - remainingLeaveAlloted;
				remainingLeaveAlloted = 0;
			}
			if( yearMonth.equals(thisMonth)) {
				model.addAttribute("remainingLeave", remainingLeaveAlloted);
			}

			BigDecimal salaryPerDay = employee.getSalary().divide(BigDecimal.valueOf(yearMonth.lengthOfMonth()), 2,
					RoundingMode.FLOOR);
			BigDecimal salaryThisMonth = employee.getSalary()
					.subtract(salaryPerDay.multiply(BigDecimal.valueOf(extraLeave)));

			row.add(yearMonth.format(DateTimeFormatter.ofPattern("MMM")));
			row.add(leaveTaken + "");
			row.add(extraLeave + "");

			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			df.setMinimumFractionDigits(0);
			df.setGroupingUsed(true);
			row.add(df.format(salaryThisMonth));

			table.add(row);

		}
		model.addAttribute("reports", table);
		
	}

	public void addReport(Model model) {
		addReport(model, employeeService.getCurrentUser());
	}

	public void addReport(Model model, Employee employee) {
		YearMonth yearMonth = YearMonth.now();
		addReport(model, employee, yearMonth.getYear());

	}

	/**
	 * Generate a report and all the required attribute to publish the report.
	 * @param model
	 * @param employee
	 * @param year
	 */
	public void addReport(Model model, Employee employee, int year) {
		model.addAttribute("year", year);
		model.addAttribute("userName", employee.getUserName());
		model.addAttribute("name", employee.getName());
		model.addAttribute("heading", Arrays.asList("Month", "Leave Taken", "Extra Leave", "Monthly Salary"));
		
		addYearlyTable(employee, year, model);
	}
}
