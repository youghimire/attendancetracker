package com.ughimire.assignment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepositories employeeRepositories;
	@Autowired
	EmployeeLeaveRepositories employeeLeaveRepositories;

	public List<Employee> getAllEmployee() {
		List<Employee> employee = new ArrayList<>();
		employeeRepositories.findAll().forEach(employee::add);
		return employee;
	}

	public List<Employee> getMyEmployee() {
		return getMyEmployee(getCurrentUser());
	}

	public List<Employee> getMyEmployee(Employee admin) {
		return admin.getEmployer().getEmployes();
	}

	public Employee findByUserName(String userName) {

		Iterable<Employee> employees = employeeRepositories.findAll();
		for (Employee employee : employees) {
			if (employee.getUserName().equals(userName)) {
				return employee;
			}
		}
		return null;

	}

	/**
	 * Get the current Employee who is logged in
	 * @return
	 */
	public Employee getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			return findByUserName(currentUserName);
		} else {
			return null;
		}
	}

	public void setLeave(Date date, int days, String employeeName) {
		employeeLeaveRepositories.save(new EmployeeLeave(null, findByUserName(employeeName), date, days));

	}
}
