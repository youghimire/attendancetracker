package com.ughimire.assignment;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Setting {

	int leavePerMonth;
	int leavePerYear;
	
}
